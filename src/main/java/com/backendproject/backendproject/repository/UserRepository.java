package com.backendproject.backendproject.repository;


import com.backendproject.backendproject.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public interface UserRepository extends JpaRepository<User,Long> {
    User findOneById(Long id);
    User findByUsername(String username);
    List<User> findByIndexNumber(Long index);
    User findFirstByUsername(String username);
    User findFirstByIndexNumber(Long indexNumber);
}
