package com.backendproject.backendproject.services;

import com.backendproject.backendproject.DTO.UserDTO;
import com.backendproject.backendproject.model.Role;
import com.backendproject.backendproject.model.User;
import com.backendproject.backendproject.repository.RoleRepository;
import com.backendproject.backendproject.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.backendproject.backendproject.model.StaticRoleNames.ADMIN_ROLE;
import static com.backendproject.backendproject.model.StaticRoleNames.USER_ROLE;

@Service
public class UserService{

    @Autowired
    private PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Autowired
    public UserService(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository= userRepository;
        this.roleRepository = roleRepository;
    }

    private User addUser(User user)  {
        Role userRole = roleRepository.findByName(USER_ROLE);
        user.getRoles().add(userRole);
        user.setPassword(user.getPassword());
        if(userRepository.findByUsername(user.getUsername()) != null)
            return null;
        User savedUser = userRepository.save(user);
        return savedUser;
    }
    public User getUserByindexNumber(Long indexNumber){
        return this.userRepository.findFirstByIndexNumber(indexNumber);
    }
    public User getUserByUsername(String useranme){
        return userRepository.findByUsername(useranme);
    }

    public User getOne(Long id){
        return userRepository.findOneById(id);
    }
    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    public void addAdmin(User user){
        Role userRole = roleRepository.findByName(ADMIN_ROLE);
        user.getRoles().add(userRole);
        userRepository.save(user);
    }

    public void addUserAdmin(User user){
        Role userRole = roleRepository.findByName(ADMIN_ROLE);
        Role adminRole= roleRepository.findByName(USER_ROLE);
        user.getRoles().add(userRole);
        user.getRoles().add(adminRole);
        user.setIndexNumber(Long.valueOf(1));
        userRepository.save(user);
    }
    public User addUser(UserDTO userDTO) {

        User user = new User(userDTO);
        return  this.addUser(user);
    }
    public User update(User user){
       return userRepository.save(user);
    }
}
